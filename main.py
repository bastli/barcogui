from server import Server
import sys
from PyQt5.QtWidgets import QApplication
from mainwindow import MainWindow

if __name__ == '__main__':

    app = QApplication(sys.argv)
    s = Server('127.0.0.1', 1336)
    main_window = MainWindow(s)
    sys.exit(app.exec_())
