from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QPushButton
from barco import Barco

class MainWindow(QWidget):

    def __init__(self, server):
        super().__init__()
        self.server = server
        self.moving = False
        self.initUI()

    def initUI(self):

        self.setGeometry(300, 300, 650, 900)
        self.setWindowTitle('Barco Mapper')

        self.physical_label = QLabel('P:', self)
        self.physical_label.move(45, 10)

        self.logical_label = QLabel('L:', self)
        self.logical_label.move(75, 10)

        self.barcos = []
        for i in range(15):
            self.barcos.append(Barco(self, i, i))
            self.barcos[-1].move(40, 40 + 40 * i)

        self.button_show_id = QPushButton('Show Physical IDs', self)
        self.button_show_id.resize(100, 30)
        self.button_show_id.move(40, 660)
        self.button_show_id.clicked.connect(self.server.show_pnumber)

        self.show()
