import socket
from struct import pack, calcsize

class Server:
    def __init__(self, ip, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(('10.6.66.10', 1338))

    def set_stripes(self, color):
        self.socket.send(pack('>I', 0))
        self.socket.send(pack('4b', color[0], color[1], color[2]))

    def set_lstripe_single(self, index, color):
        self.socket.send(pack('>I', 2))
        self.socket.send(pack('4b', index, color[0], color[1], color[2]))

    def set_lstripe(self, index, *pixel_colors):
        self.socket.send(pack('>I', 0xb16b00b5))
        self.socket.send(pack('337b', index, *pixel_colors)) # 112 * 3 + 1

    def show_pnumber(self):
        self.socket.send(pack('>I', 0xfefefefe))

    def stop(self):
        self.socket.send(pack('>I', 1))

    def set_lstripe_single(self, index, color):
        self.socket.send(pack('>I', 0xcafebabe))
        self.socket.send(pack('4b', index, color[0], color[1], color[2]))

    def map_logical_to_physical(self, logical, physical):
        self.socket.send(pack('>I', 0xdeadbeef))
        self.socket.send(pack('2b', logical, physical))

    def stop(self):
        self.socket.close()
