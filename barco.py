from PyQt5.QtWidgets import QWidget, QLabel, QHBoxLayout, QPushButton, QLineEdit, QSpacerItem
from PyQt5.QtGui import QPainter, QColor, QBrush, QDrag, QPixmap, QIcon, QPalette
from PyQt5.QtCore import Qt, QMimeData


class Barco(QWidget):

    def __init__(self, parent, physical_id, logical_id):
        super(Barco, self).__init__(parent)
        self.color = QColor(200, 0, 0, 255)
        self.h = 30
        self.w = 448
        self.x = 60
        self.y = 0
        self.fill_percentage = 100
        self.physical_id = physical_id
        self.logical_id = logical_id

        self.setStyleSheet(
            '''QWidget {
                margin: 0;
                margin: 0px;
                border: 0px;
                padding: 0px;
                border-style: none;
            }'''
        )

        self.physical_id_input = QLineEdit(str(self.physical_id), self)
        self.physical_id_input.setStyleSheet(
            '''QLineEdit {
                background: #FF530D;
                selection-background-color: #FF530D;
                margin: 0;
                margin: 0px;
                border: 0px;
                padding: 0px;
                border-style: none;
            }'''
        )
        self.physical_id_input.resize(30, 30)
        self.physical_id_input.move(0, 0)
        self.physical_id_input.setEnabled(False)

        self.logical_id_input = QLineEdit(str(self.logical_id), self)
        self.logical_id_input.setStyleSheet(
            '''QLineEdit {
                background: #0EAFFF;
                selection-background-color: #0EAFFF;
                margin: 0px;
                border: 0px;
                padding: 0px;
                border-style: none;
            }'''
        )
        self.logical_id_input.resize(30, 30)
        self.logical_id_input.move(30, 0)
        self.logical_id_input.setEnabled(False)

        pixmap= QPixmap("img/pencil.png");
        icon = QIcon(pixmap);
        self.button_edit = QPushButton(self)
        self.button_edit.setIcon(icon);
        self.button_edit.setIconSize(pixmap.rect().size());
        self.button_edit.setFixedSize(pixmap.rect().size());
        self.button_edit.setFlat(True)
        self.button_edit.clicked.connect(self.on_click_edit)
        self.button_edit.move(520, 7)
        self.button_edit.setEnabled(True)

        pixmap= QPixmap("img/tick.png");
        icon = QIcon(pixmap);
        self.button_accept = QPushButton(self)
        self.button_accept.setIcon(icon);
        self.button_accept.setIconSize(pixmap.rect().size());
        self.button_accept.setFixedSize(pixmap.rect().size());
        self.button_accept.setFlat(True)
        self.button_accept.clicked.connect(self.on_click_accept)
        self.button_accept.move(520, 7)
        self.button_accept.hide()
        self.button_accept.setEnabled(False)

        #self.setAcceptDrops(True)
        self.moving = False

    def paintEvent(self, e):

        qp = QPainter()
        qp.begin(self)
        self.drawRectangles(qp)
        qp.end()

    def drawRectangles(self, qp):

        col = QColor(0, 0, 0)
        col.setNamedColor('#d4d4d4')
        qp.setPen(col)

        qp.setBrush(self.color)
        qp.drawRect(self.x + self.w * (100 - self.fill_percentage) / 100, self.y, self.w * self.fill_percentage / 100, self.h)

    def set_color(self, r, g, b, a):
        self.color = QColor(r, g, b, a)

    def set_physical_id(self, physical_id):
        self.physical_id = physical_id
        self.physical_label.setText(str(physical_id))

    def on_click_edit(self):
        self.button_edit.hide()
        self.button_edit.setEnabled(False)
        self.button_accept.show()
        self.button_accept.setEnabled(True)
        self.logical_id_input.setEnabled(True)

    def on_click_accept(self):
        self.logical_id = int(self.logical_id_input.text())
        self.physical_id = int(self.physical_id_input.text())
        self.parent().server.map_logical_to_physical(self.logical_id, self.physical_id)
        self.button_accept.hide()
        self.button_accept.setEnabled(False)
        self.button_edit.show()
        self.button_edit.setEnabled(True)
        self.logical_id_input.setEnabled(False)

    # def mouseMoveEvent(self, e):
    #     if e.buttons() != Qt.LeftButton or self.moving or self.parent().moving:
    #         return
    #
    #     mimeData = QMimeData()
    #     drag = QDrag(self)
    #     drag.setMimeData(mimeData)
    #     drag.setHotSpot(e.pos() - self.rect().topLeft())
    #     dropAction = drag.exec_(Qt.MoveAction)
    #     print('qdrag deployed')
    #
    # def dragEnterEvent(self, e):
    #     if not self.moving and not self.parent().moving:
    #         self.moving = True
    #         self.parent().moving = True
    #         print(self.physical_id)
    #         e.accept()
    #         print('drag accepted')
    #     else:
    #         e.ignore()
    #
    # def dropEvent(self, e):
    #     position = e.pos()
    #     print('keke')
    #     self.barcos.remove(self)
    #     self.parent().layout.removeWidget(self)
    #
    #     print(self.physical_id)
    #     print(len(self.barcos))
    #     for key, barco in enumerate(self.barcos):
    #         #print(key, barco.rect().topLeft().y())
    #         if position.y() > barco.rect().topLeft().y() + barco.h / 2 and (key == len(self.barcos) - 1 or position.y() < self.barcos[key + 1].rect().topLeft().y() + barco.h / 2):
    #             self.barcos.insert(key, self)
    #             self.parent().layout.insertWidget(key, self)
    #             break
    #         if position.y() < barco.rect().topLeft().y() + barco.h / 2 and (key == 0 or position.y() > self.barcos[key - 1].rect().topLeft().y() + barco.h / 2):
    #             self.barcos.insert(key, self)
    #             self.parent().layout.insertWidget(key, self)
    #             break
    #     print('keke')
    #     self.moving = False
    #     self.parent().moving = False
    #     e.setDropAction(Qt.MoveAction)
    #     e.accept()
